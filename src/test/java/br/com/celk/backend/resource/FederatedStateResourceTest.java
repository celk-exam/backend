package br.com.celk.backend.resource;

import br.com.celk.backend.exception.AlreadyExistsException;
import br.com.celk.backend.exception.FederatedStateException;
import br.com.celk.backend.exception.NotFoundException;
import br.com.celk.backend.model.FederatedState;
import br.com.celk.backend.model.dto.FederatedStateDTO;
import br.com.celk.backend.util.CustomPageImpl;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc
@TestExecutionListeners(
        listeners = {TransactionDbUnitTestExecutionListener.class, DirtiesContextTestExecutionListener.class},
        mergeMode = TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS
)
class FederatedStateResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @DatabaseSetup(value = {"/dataset/federated-states.xml"})
    void findAll() throws Exception {
        MvcResult result = mockMvc.perform(get("/federated-states")).andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());

        CustomPageImpl<FederatedState> federatedStates = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<CustomPageImpl<FederatedState>>() {
        });

        assertEquals(4, federatedStates.getTotalElements());
        assertTrue(federatedStates.getContent().stream().anyMatch(federatedState -> federatedState.getCode().equals("SC")));
        assertTrue(federatedStates.getContent().stream().anyMatch(federatedState -> federatedState.getCode().equals("MS")));
        assertTrue(federatedStates.getContent().stream().anyMatch(federatedState -> federatedState.getCode().equals("PR")));
        assertTrue(federatedStates.getContent().stream().anyMatch(federatedState -> federatedState.getCode().equals("MT")));
        assertFalse(federatedStates.getContent().stream().anyMatch(federatedState -> federatedState.getCode().equals("SP")));
    }

    @Test
    @DatabaseSetup(value = {"/dataset/federated-states.xml"})
    void findAll_query() throws Exception {
        MvcResult result = mockMvc.perform(get("/federated-states?query=mato")).andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());

        CustomPageImpl<FederatedState> federatedStates = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<CustomPageImpl<FederatedState>>() {
        });

        assertEquals(2, federatedStates.getTotalElements());
        assertTrue(federatedStates.getContent().stream().anyMatch(federatedState -> federatedState.getCode().equals("MS")));
        assertTrue(federatedStates.getContent().stream().anyMatch(federatedState -> federatedState.getCode().equals("MT")));
        assertFalse(federatedStates.getContent().stream().anyMatch(federatedState -> federatedState.getCode().equals("SC")));
        assertFalse(federatedStates.getContent().stream().anyMatch(federatedState -> federatedState.getCode().equals("PR")));
    }

    @Test
    @DatabaseSetup(value = {"/dataset/federated-states.xml"})
    void findByCode_ok() throws Exception {
        MvcResult result = mockMvc.perform(get("/federated-states/sc")).andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        FederatedState federatedState = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<FederatedState>() {
        });

        assertNotNull(federatedState);
        assertEquals("SC", federatedState.getCode());
        assertEquals("Santa Catarina", federatedState.getName());
    }

    @Test
    @DatabaseSetup(type = DatabaseOperation.TRUNCATE_TABLE)
    void findByCode_not_found() throws Exception {
        MvcResult result = mockMvc.perform(get("/federated-states/rj")).andReturn();

        assertEquals(HttpStatus.NOT_FOUND.value(), result.getResponse().getStatus());
        assertNotNull(result.getResolvedException());
        assertTrue(result.getResolvedException() instanceof NotFoundException);
    }

    @Test
    @DatabaseSetup(type = DatabaseOperation.TRUNCATE_TABLE)
    void create_ok() throws Exception {
        FederatedStateDTO dto = FederatedStateDTO.builder().code("RJ").name("Rio de Janeiro").build();

        MvcResult result = mockMvc.perform(post("/federated-states").content(objectMapper.writeValueAsString(dto)).contentType(MediaType.APPLICATION_JSON)).andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        FederatedState federatedState = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<FederatedState>() {
        });

        assertNotNull(federatedState.getId());
        assertEquals("RJ", federatedState.getCode());
        assertEquals("Rio de Janeiro", federatedState.getName());
        assertNotNull(federatedState.getCreatedDate());
    }

    @Test
    @DatabaseSetup(value = {"/dataset/federated-states.xml"})
    void create_repeated_code() throws Exception {
        FederatedStateDTO dto = FederatedStateDTO.builder().code("MS").name("Mato Grosso do Sul").build();

        MvcResult result = mockMvc.perform(post("/federated-states").content(objectMapper.writeValueAsString(dto)).contentType(MediaType.APPLICATION_JSON)).andReturn();

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), result.getResponse().getStatus());
        assertNotNull(result.getResolvedException());
        assertEquals("Federated state code MS already exists", result.getResolvedException().getMessage());
        assertTrue(result.getResolvedException() instanceof AlreadyExistsException);
    }

    @Test
    @DatabaseSetup(value = {"/dataset/federated-states.xml"})
    void edit_ok() throws Exception {
        FederatedStateDTO dto = FederatedStateDTO.builder().id(2L).code("MS").name("Mato Grosso do Sul!").build();

        MvcResult result = mockMvc.perform(put("/federated-states/2").content(objectMapper.writeValueAsString(dto)).contentType(MediaType.APPLICATION_JSON)).andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        FederatedState federatedState = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<FederatedState>() {
        });

        assertEquals("MS", federatedState.getCode());
        assertEquals("Mato Grosso do Sul!", federatedState.getName());
    }

    @Test
    @DatabaseSetup(type = DatabaseOperation.TRUNCATE_TABLE)
    void edit_nok_not_found() throws Exception {
        FederatedStateDTO dto = FederatedStateDTO.builder().id(2L).code("MS").name("Mato Grosso do Sul!").build();

        MvcResult result = mockMvc.perform(put("/federated-states/2").content(objectMapper.writeValueAsString(dto)).contentType(MediaType.APPLICATION_JSON)).andReturn();

        assertEquals(HttpStatus.NOT_FOUND.value(), result.getResponse().getStatus());
        assertNotNull(result.getResolvedException());
        assertTrue(result.getResolvedException() instanceof NotFoundException);
    }

    @Test
    @DatabaseSetup(value = {"/dataset/federated-states.xml"})
    void edit_nok_id_does_not_match_with_entity() throws Exception {
        FederatedStateDTO dto = FederatedStateDTO.builder().id(2L).code("MS").name("Mato Grosso do Sul!").build();

        MvcResult result = mockMvc.perform(put("/federated-states/5").content(objectMapper.writeValueAsString(dto)).contentType(MediaType.APPLICATION_JSON)).andReturn();

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), result.getResponse().getStatus());
        assertNotNull(result.getResolvedException());
        assertTrue(result.getResolvedException() instanceof FederatedStateException);
        assertEquals("Federated state with id 5 does not match with previous saved id 2", result.getResolvedException().getMessage());
    }

    @Test
    @DatabaseSetup(value = {"/dataset/federated-states.xml"})
    @ExpectedDatabase(value = "/dataset/federated-states-expected.xml")
    void delete_ok() throws Exception {
        MvcResult result = mockMvc.perform(delete("/federated-states/2")).andReturn();
        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
    }

    @Test
    @DatabaseSetup(type = DatabaseOperation.TRUNCATE_TABLE)
    void delete_nok_not_found() throws Exception {
        MvcResult result = mockMvc.perform(delete("/federated-states/1")).andReturn();
        assertEquals(HttpStatus.NOT_FOUND.value(), result.getResponse().getStatus());
        assertNotNull(result.getResolvedException());
        assertTrue(result.getResolvedException() instanceof NotFoundException);
    }
}
