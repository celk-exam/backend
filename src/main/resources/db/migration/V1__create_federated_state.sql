create table federated_state
(
	id serial not null,
	code varchar(2) not null,
	name varchar(255) not null,
	created_date timestamp not null
);

create unique index federated_state_id_uindex
	on federated_state (id);

alter table federated_state
	add constraint federated_state_pk
		primary key (id);
