package br.com.celk.backend.config;

import br.com.celk.backend.BackendApplication;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Profile("!test")
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Value("${application.version}")
    private String projectVersion;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(BackendApplication.class.getPackage().getName()))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo("Federated state API",
                "Federated state API.",
                projectVersion,
                null,
                new Contact("Rafael Ramos", "http://github.com/arkanjoms", "arkanjo.ms@gmail.com"),
                "MIT",
                "https://mit-license.org",
                Collections.emptyList());
    }
}
