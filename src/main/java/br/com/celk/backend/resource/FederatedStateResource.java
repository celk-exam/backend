package br.com.celk.backend.resource;

import br.com.celk.backend.exception.AlreadyExistsException;
import br.com.celk.backend.exception.FederatedStateException;
import br.com.celk.backend.exception.NotFoundException;
import br.com.celk.backend.model.dto.FederatedStateDTO;
import br.com.celk.backend.model.dto.FederationStateProjection;
import br.com.celk.backend.service.FederatedStateService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping("/federated-states")
public class FederatedStateResource {

    private final FederatedStateService service;

    public FederatedStateResource(FederatedStateService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<Page<FederationStateProjection>> findAll(@RequestParam(value = "query", required = false) String query, Pageable page) {
        return ResponseEntity.ok(service.findAll(query, page));
    }

    @GetMapping("/{code}")
    public ResponseEntity<FederationStateProjection> findByCode(@PathVariable("code") String code) throws NotFoundException {
        return ResponseEntity.ok(service.findByCode(code));
    }

    @PostMapping
    public ResponseEntity<FederatedStateDTO> create(@Valid @RequestBody FederatedStateDTO federatedState) throws AlreadyExistsException {
        return ResponseEntity.ok(service.create(federatedState));
    }

    @PutMapping("/{id}")
    public ResponseEntity<FederatedStateDTO> edit(@PathVariable("id") Long id, @RequestBody FederatedStateDTO federatedState) throws FederatedStateException, NotFoundException {
        return ResponseEntity.ok(service.edit(id, federatedState));
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) throws NotFoundException {
        service.delete(id);
    }
}
