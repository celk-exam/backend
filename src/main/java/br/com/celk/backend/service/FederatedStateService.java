package br.com.celk.backend.service;

import br.com.celk.backend.exception.AlreadyExistsException;
import br.com.celk.backend.exception.FederatedStateException;
import br.com.celk.backend.exception.NotFoundException;
import br.com.celk.backend.model.FederatedState;
import br.com.celk.backend.model.dto.FederatedStateDTO;
import br.com.celk.backend.model.dto.FederationStateProjection;
import br.com.celk.backend.model.mapper.FederatedStateMapper;
import br.com.celk.backend.repository.FederatedStateRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class FederatedStateService {

    private final FederatedStateRepository repository;

    public FederatedStateService(FederatedStateRepository repository) {
        this.repository = repository;
    }

    public Page<FederationStateProjection> findAll(String query, Pageable page) {
        if (StringUtils.isEmpty(query)) {
            return repository.findAllProjectedBy(page);
        }
        return repository.findAllProjectedByNameContainingIgnoreCase(query, page);
    }

    public FederationStateProjection findByCode(String code) throws NotFoundException {
        return repository.findProjectedByCodeIgnoreCase(code)
                .orElseThrow(NotFoundException::new);
    }

    public FederatedStateDTO create(FederatedStateDTO dto) throws AlreadyExistsException {
        if (repository.existsByCode(dto.getCode())) {
            throw new AlreadyExistsException(MessageFormat.format("Federated state code {0} already exists", dto.getCode()));
        }
        dto.setCreatedDate(LocalDateTime.now());
        FederatedState savedEntity = repository.save(FederatedStateMapper.toEntity(dto));
        return FederatedStateMapper.toDTO(savedEntity);
    }

    public FederatedStateDTO edit(Long id, FederatedStateDTO dto) throws FederatedStateException, NotFoundException {
        if (!repository.existsByCode(dto.getCode())) {
            throw new NotFoundException();
        }

        Optional<FederatedState> oldEntity = repository.findById(id);
        if (oldEntity.isPresent() && oldEntity.get().getCode().equals(dto.getCode())) {
            FederatedState savedEntity = repository.save(FederatedStateMapper.toEntity(dto));
            return FederatedStateMapper.toDTO(savedEntity);
        }

        throw new FederatedStateException(MessageFormat.format("Federated state with id {0} does not match with previous saved id {1}", id, dto.getId()));
    }

    public void delete(Long id) throws NotFoundException {
        if (!repository.existsById(id)) {
            throw new NotFoundException();
        }

        repository.deleteById(id);
    }
}
