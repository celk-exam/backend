package br.com.celk.backend.model.mapper;

import br.com.celk.backend.model.FederatedState;
import br.com.celk.backend.model.dto.FederatedStateDTO;

public class FederatedStateMapper {

    private FederatedStateMapper() {
        super();
    }

    public static FederatedStateDTO toDTO(FederatedState entity) {
        return FederatedStateDTO.builder()
                .id(entity.getId())
                .code(entity.getCode())
                .name(entity.getName())
                .createdDate(entity.getCreatedDate())
                .build();
    }

    public static FederatedState toEntity(FederatedStateDTO dto) {
        return FederatedState.builder()
                .id(dto.getId())
                .code(dto.getCode())
                .name(dto.getName())
                .createdDate(dto.getCreatedDate())
                .build();
    }
}
