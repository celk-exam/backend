package br.com.celk.backend.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

import java.time.LocalDateTime;

public interface FederationStateProjection {

    Long getId();

    String getCode();

    String getName();

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    LocalDateTime getCreatedDate();
}
