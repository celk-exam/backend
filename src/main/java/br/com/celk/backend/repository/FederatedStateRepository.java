package br.com.celk.backend.repository;

import br.com.celk.backend.model.FederatedState;
import br.com.celk.backend.model.dto.FederationStateProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FederatedStateRepository extends JpaRepository<FederatedState, Long> {

    Page<FederationStateProjection> findAllProjectedBy(Pageable pageable);

    Page<FederationStateProjection> findAllProjectedByNameContainingIgnoreCase(String name, Pageable pageable);

    Optional<FederationStateProjection> findProjectedByCodeIgnoreCase(String code);

    boolean existsByCode(String code);
}
