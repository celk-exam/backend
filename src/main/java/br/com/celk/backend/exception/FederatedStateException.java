package br.com.celk.backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public class FederatedStateException extends Exception {

    private static final long serialVersionUID = 8967545859740759906L;

    public FederatedStateException(String s) {
        super(s);
    }
}
