package br.com.celk.backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public class AlreadyExistsException extends Exception {

    private static final long serialVersionUID = -1050883062881106532L;

    public AlreadyExistsException(String s) {
        super(s);
    }
}
