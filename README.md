# backend

[Swagger](https://federated-states-backend.herokuapp.com/swagger-ui.html)

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=celk-exam_backend&metric=alert_status)](https://sonarcloud.io/dashboard?id=celk-exam_backend)

Backend app para gerenciamento de Unidades Federativas

## Tecnologias

Tecnologias utilizadas no desenvolvimento

 - Java 8
 - maven
 - spring-boot
 - Swagger
 - hibernate
 - JPA
 - Postgres
 - Flyway
 - Docker
 - Gitlab-CI
 - Heroku

## Executando ambiente de desenvolvimento

Realizar clone do projeto e iniciar uma instância do postgresql, atualizando o arquivo `src/main/resources/application.properties` com as credenciais de acesso ao banco de dados.

Após a configuração do banco de dados, executar o comando abaixo:
```bash
$ mvn spring-boot:run
```
