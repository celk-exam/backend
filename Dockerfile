FROM openjdk:8-alpine

ENV JAVA_OPTS="-Xms64m -Xmx96m -XX:MetaspaceSize=64m -XX:MaxMetaspaceSize=96m"

RUN addgroup -S celk \
    && adduser -S -G celk celk \
    && mkdir /app && chown -R celk:celk /app

USER celk

COPY target/backend.jar /app/

WORKDIR /app

EXPOSE 8080

CMD java -Djava.security.egd=file:/dev/./urandom -server -XX:+UseContainerSupport \
    ${JAVA_OPTS} -jar backend.jar